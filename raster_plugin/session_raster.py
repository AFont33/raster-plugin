# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pyforms import conf

import numpy as np

from AnyQt.QtWidgets import QMessageBox

from AnyQt.QtGui import QColor, QBrush
from AnyQt.QtCore import QTimer, QEventLoop, QAbstractTableModel, Qt, QSize, QVariant, pyqtSignal

from pybpodapi.session import Session
from pyforms import BaseWidget
from pyforms.controls import ControlProgress
from pyforms.controls import ControlText
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel
from pyforms.controls import ControlButton
from pyforms.controls import ControlCheckBox
from pyforms.controls import ControlCombo

from raster_plugin.exc_manager import log
from raster_plugin.raster_data import RasterData

import matplotlib.gridspec as gridspec
from matplotlib.lines import Line2D
from matplotlib.ticker import MaxNLocator

import logging
import traceback
import datetime

import os
import json

logger = logging.getLogger(__name__)


class SessionRaster(BaseWidget):
    """ Plugin main window """
    
    @log
    def __init__(self, session):
        BaseWidget.__init__(self, session.name)
        self.layout().setContentsMargins(5, 5, 5, 5)  # Set graphic margin
        self.session = session  # Session class of the pybpod to get data
        self.LAST_POSITION = 0  # This variable records the last received dataframe index
        self.trials_to_do = []
        # --------- Form creation: --------------
        self._graph = ControlMatplotlib('Graph')
        self._updatebutton = ControlButton('Update')
        self._valGraph = ControlText('Values to show: ', default='30', helptext='Number of trials that are being plotted in the graph.')
        self._titleTrend = ControlLabel('Raster visualization.')
        self._task_settings = ControlCombo('Presets: ', helptext='The settings for this session.')
        # Set the task list:
        path = os.path.join(conf.GENERIC_EDITOR_PLUGINS_PATH, 'raster-plugin', 'raster_plugin', 'plot_settings.json')
        with open(path) as settings_file:
            tasks = json.load(settings_file).keys()
        for elem in tasks:
            self._task_settings.add_item(elem, elem)
        # Form position:
        self._formset = [
            ('_titleTrend', '_valGraph','_updatebutton', '_task_settings'),
            '_graph',
        ]
        # Assign an action to the button
        self._updatebutton.value = self.__updateAction
        # Set the timer to recall the function update
        self._timer = QTimer()
        self._timer.timeout.connect(self.update)
        try:
            self._setup_name = self.session.data[self.session.data.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
        except IndexError:
            self._setup_name = "Unknown box"
        try:
            self._session_started = self.session.data[self.session.data.MSG == 'SESSION-STARTED']['+INFO'].iloc[0].split()[1][0:8]
        except IndexError:
            self._session_started = "Unknown time"
        
        try:
            self._subject_name = self.session.data[self.session.data.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
            self._subject_name = self._subject_name[1:-1].split(',')[0].strip("'")
        except IndexError:
            self._subject_name = "Unknown subject"
        self.current_task = self._task_settings.value
        # RasterData class that will make most of the preparations for the plots:
        self.processor = RasterData(self._valGraph.value, self.current_task)
        self._graph.value = self.__on_draw
        self.title = f'{self._setup_name} / {self._session_started} / {self._subject_name}'

    def __updateAction(self):
        """ 
        Action tied to the update button; the user should cklick after changing the number of trials on the plot
        or after selecting a different preset.
        """
        # Changing the settings implies hving to recalculate everything, and it can be a bit slow.
        # Thus, make sure to only change the settings if the settings have changed (obvious, right?)
        if self.current_task != self._task_settings.value:
            self.processor.change_settings(self._task_settings.value)
            self.current_task = self._task_settings.value 
            self.LAST_POSITION = 0
            self.trials_to_do = []
        # Check the value and update the graph:
        try:
            v = int(self._valGraph.value)  # Transform the string value to int
            if v > 0:  # It's ok only in this case
                self.processor.axis_size = v
                self.read_message_queue()
                self._graph.draw()
            else:  # Otherwise show poupup message. 
                self.warning('Only integers greater than 0 are allowed.', 'ValueError')
        except ValueError:
            self.warning('Only integers greater than 0 are allowed.', 'ValueError')

    @log
    def read_message_queue(self):
        """ 
        Reads the new data coming from BPOD and gets the index of the past completed
        trials. Then sends the completed trials to RasterData for processing, and then the
        current trial separately.
        """
        try:
            dataframe = self.session.data[self.LAST_POSITION:]
            new_trials = dataframe.query("TYPE=='TRIAL' and MSG=='New trial'").index
            trials_to_do = np.concatenate((self.trials_to_do, new_trials), axis=None)
            # Send past completed trials for processing:
            for band in [trials_to_do[i:i+2] for i in range(len(trials_to_do) - 1)]:
                if len(band) != 2:
                    continue
                start, end = band
                trial = self.session.data[int(start):int(end)]
                print(trial, trials_to_do)
                self.processor.process_past_trials(trial)
            last_trial = int(trials_to_do[-1])
            self.trials_to_do = [last_trial]
            # Send current (incomplete) trial for processing:
            current_trial = self.session.data[last_trial:]
            self.processor.process_current_trial(current_trial)
            self.LAST_POSITION += len(dataframe)
            self._graph.draw()
        # If there's an error in the plot, save the traceback into a file for later check.
        # Catching general Exceptions is bad, but in this case it's fine.
        except Exception as err:
            if hasattr(self, '_timer'):
                self._timer.stop()
            # logger.error(str(err), exc_info=True)
            traceback.print_tb(err.__traceback__)
            with open('plugins_logs.txt', 'a') as logfile:
                    logfile.write(str(datetime.datetime.now())+'\n')
                    logfile.write(traceback.format_exc())
                    logfile.write('-'*25)
            QMessageBox.critical(self, "Error",
                                 "An error in the trend plugin occurred. Check logs for details.")

    @log
    def update(self):
        """ 
        This method is tied to a Qtimer that keeps calling it every X seconds,
        where X is a number defined in the settings. Its main job is to periodically 
        refresh the plot."""
        if not self.session.is_running: 
            self._timer.stop()
        if len(self.session.data) > self.LAST_POSITION:
            self.read_message_queue()

    @log
    def __on_draw(self, figure):
        """ Redraws the figure """
        figure.clear()
        # Define two plots with GridSpec, a very narrow one for the current plot
        # and a wider one for the past trials visualization:
        gs = gridspec.GridSpec(5,1)
        axis1 = figure.add_subplot(gs[0,0])
        axis2 = figure.add_subplot(gs[1:, 0])
        # Gather and plot all the past states:
        states = self.processor.past_states
        axis2.barh(states['yaxis'], 
                states['length'], 
                left=states['start_times'], 
                color=states['color'], 
                alpha=0.5, zorder=1)
        axis2.set_xlim([-3,4]) 
        if self.processor.axis_size > self.processor.END:
            axis2.set_ylim([self.processor.START, self.processor.axis_size])
        else:
            axis2.set_ylim([self.processor.START, self.processor.END+1])
        # Gather and plot all past pokes:
        pokes_in, pokes_out = self.processor.past_pokes
        axis2.scatter(pokes_in['start_times'],
                pokes_in['yaxis'], 
                color=pokes_in['color'], 
                s=5, zorder=2) 
        axis2.scatter(pokes_out['start_times'], 
                pokes_out['yaxis'], 
                color=pokes_out['color'], 
                s=5, zorder=2, marker='|') 
        axis2.set_xlabel('Time [s]')
        axis2.set_ylabel('Trials')
        axis2.yaxis.set_major_locator(MaxNLocator(integer=True))
        # Gather and plot current states and pokes:
        current_trial_states = self.processor.current_trial
        current_pokes_in, current_pokes_out = self.processor.current_pokes
        if current_trial_states['yaxis'] is not None: # Current trial can oftentimes have no information to display.
            axis1.barh(current_trial_states['yaxis'], 
                    current_trial_states['length'], 
                    left=current_trial_states['start_times'], 
                    color=current_trial_states['color'], 
                    alpha=0.6, zorder=1, height=2) 
        axis1.scatter(current_pokes_in['start_times'], 
                current_pokes_in['yaxis'], 
                color=current_pokes_in['color'],
                s=8, zorder=2) 
        axis1.scatter(current_pokes_out['start_times'], 
                current_pokes_out['yaxis'], 
                color=current_pokes_out['color'],
                s=8, zorder=2, marker='|') 
        axis1.spines['top'].set_visible(False)
        axis1.spines['right'].set_visible(False)
        axis1.set_yticks([self.processor.COUNT])
        axis1.set_ylim([self.processor.COUNT-1, self.processor.COUNT+1])
        
        figure.tight_layout()

    @log
    def show(self, detached=False):
        """ Method used to show the windows """
        # Prevent the call to be recursive because of the mdi_area
        if not detached:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called
        else:
            BaseWidget.show(self)
        
        self._stop = False  # flag used to close the gui in the middle of a loading
        self.read_message_queue() # Read all the data and update the plot
        
        if not self._stop and self.session.is_running: # if it is in real time
            self._timer.start(conf.SESSIONTREND_PLUGIN_REFRESH_RATE)

    def before_close_event(self):
        """ Method called before closing the windows. """
        self._timer.stop()
        self._stop = True
        self.session.sessiontrend_action.setEnabled(True)
        self.session.sessiontrend_detached_action.setEnabled(True)

    @property
    def mainwindow(self):
        return self.session.mainwindow

    @property
    def title(self):
        return BaseWidget.title.fget(self)

    @title.setter
    def title(self, value):
        BaseWidget.title.fset(self, value)


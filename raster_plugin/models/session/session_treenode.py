# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pyforms import conf

from AnyQt.QtGui import QIcon
from raster_plugin.session_raster import SessionRaster
from raster_plugin.exc_manager import log

logger = logging.getLogger(__name__)


class SessionTreeNode(object):
    @log
    def create_treenode(self, tree):
        """

        :param tree:
        :return:
        """
        node = super(SessionTreeNode, self).create_treenode(tree)

        self.sessionraster_action = tree.add_popup_menu_option(
            'Raster',
            self.open_sessionraster_win,
            item=self.node,
            icon=QIcon(conf.SESSIONRASTER_PLUGIN_ICON)
        )

        self.sessionraster_detached_action = tree.add_popup_menu_option(
            'Raster (detached)',
            self.open_sessionraster_win_detached,
            item=self.node,
            icon=QIcon(conf.SESSIONRASTER_PLUGIN_ICON)
        )

        return node

    def node_double_clicked_event(self):
        super(SessionTreeNode, self).node_double_clicked_event()
        self.open_sessionraster_win()

    def open_sessionraster_win(self):
        if self.is_running and self.setup.detached: # if the detached flag is anable don't show the data
            return

        self.load_contents()
        # does not show the window if the detached window is visible
        if hasattr(self, 'raster_win_detached') and self.sessionraster_win_detached.visible: return

        if not hasattr(self, 'raster_win'):
            self.sessionraster_win = SessionRaster(self)
            self.sessionraster_win.show()
            self.sessionraster_win.subwindow.resize(*conf.SESSIONTREND_PLUGIN_WINDOW_SIZE)
        else:
            self.sessionraster_win.show()

        self.sessionraster_action.setEnabled(False)
        self.sessionraster_detached_action.setEnabled(False)

    def open_sessionraster_win_detached(self):
        if self.is_running and self.setup.detached:
            return
        self.load_contents()
        # does not show the window if the attached window is visible
        if hasattr(self, 'raster_win') and self.raster_win.visible: return

        if not hasattr(self, 'raster_win_detached'):
            self.raster_win_detached = SessionRaster(self)
            self.raster_win_detached.show(True)
            self.raster_win_detached.resize(*conf.SESSIONTREND_PLUGIN_WINDOW_SIZE)
        else:
            self.raster_win_detached.show(True)

        self.sessionraster_action.setEnabled(False)
        self.sessionraster_detached_action.setEnabled(False)
    @log
    def remove(self):
        if hasattr(self, 'sessionraster_win'): self.mainwindow.mdi_area -= self.sessionraster_win
        super(SessionTreeNode, self).remove()

    @property
    def name(self):
        return super(SessionTreeNode, self.__class__).name.fget(self)

    @name.setter
    def name(self, value):
        super(SessionTreeNode, self.__class__).name.fset(self, value)
        if hasattr(self, 'sessionraster_win'): self.sessionraster_win.title = value

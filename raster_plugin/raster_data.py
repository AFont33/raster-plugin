import numpy as np
from pyforms import conf
import json
import os
from types import SimpleNamespace
from raster_plugin.exc_manager import log

class Settings():
    """
    This class holds the settings for the current visualization. As explained in the README,
    the settings are saved in a JSON file in the plugin folder and have to be set up by the user before
    using the plugin.
    """

    def __init__(self, task_name):
        self.read_json_settings(task_name)

    @log
    def read_json_settings(self, task_name: str):
        """
        Read the JSON settings file and convert it to a namespace for easy acces in the main RasterData 
        class.
        """
        path = os.path.join(conf.GENERIC_EDITOR_PLUGINS_PATH, 'raster-plugin', 'raster_plugin', 'plot_settings.json')
        with open(path) as settings_file:
            self.settings = SimpleNamespace(**json.load(settings_file)[task_name])

    @log
    def change_settings(self, task_name: str):
        self.read_json_settings(task_name)

class RasterData():
    """
    This class does the heavy-lifting on the data processing that's coming from the raster windows.
    It basically parses trials coming from BPOD in a way that makes them easy to plot in the scatter and bar plots of the
    main window. It has to compute four things: states and pokes in the already completed trials, and states and pokes
    in the current trial. The former can be aligned to a certain state defined in the settings, while the latter cannot
    (no alignment is possible if the trial hasn't finished, since we always align to the latest occurence of the alignment
    state). It stores all the data into the arrays defined inside __init__, which is necessary if the user has
    to change the settings on the fly. The arrays are then processed, sliced, concatenated and sent to the plot.
    """

    def __init__(self, axis_size: str, task_name: str):
        self.init_dicts()
        self.START = 0  # First trial to plot.
        self.END = 1  # Last trial to plot. Together with START they form a slicing range that defines the plot's Y domain.
        self.axis_size = int(axis_size)
        self.COUNT = 1  # Counter that keeps track of the current trial number.
        self.settings = Settings(task_name)  # Initialize the settings.
        self.st = self.settings.settings

    def init_dicts(self):
        """
        This method resets the class data; it's called upon initialization but also after a settings change.
        """
        self.pokes_in = {'start_times': [], 'color': [], 'yaxis': []}
        self.pokes_out = {'start_times': [], 'color': [], 'yaxis': []}
        self.states = {'start_times': [], 'color': [], 'length': [], 'yaxis': []}
        self.current_trial_states = {'start_times': [], 'color': [], 'length': [], 'yaxis': []}
        self.current_trial_pokes_in = {'start_times': [], 'color': [], 'length': [], 'yaxis': []}
        self.current_trial_pokes_out = {'start_times': [], 'color': [], 'length': [], 'yaxis': []}
        self.offset_record = []

    @log
    def change_settings(self, task_name):
        self.settings.change_settings(task_name)
        self.st = self.settings.settings
        self.init_dicts()
        self.COUNT = 1
        self.END = 1
        self.START = 0

    def change_alignment(self):
        """
        To implement in the future, with a separate alignment control.
        """
        pass

    def process_past_trials(self, trial):
        """
        This method receives trials from the main window and obtains the states and pokes data by parsing them.
        It uses the STATE rows for the states (since these trials have already finished) and calls process_pokes()
        to parse the pokes.
        """
        # ------------ STATES: ------------------------------
        trial_length = []
        trial_start_times = []
        trial_color = []
        for state in self.st.state_list:
            initials = trial.query("TYPE=='STATE' and MSG==@state")['BPOD-INITIAL-TIME'].values.astype(float)
            trial_start_times = np.concatenate((trial_start_times, initials), axis=None)
            finals = trial.query("TYPE=='STATE' and MSG==@state")['BPOD-FINAL-TIME'].values.astype(float)
            trial_length = np.concatenate((trial_length, finals - initials), axis=None)
            trial_color += [self.st.state_colors[state]] * len(initials) 
        # Compute offset, if necessary:
        if self.st.alignment is not None:
            alignment_color = self.st.state_colors[self.st.alignment]
            offset_index = len(trial_color) - trial_color[::-1].index(alignment_color) - 1
            offset = trial_start_times[offset_index]
            self.offset_record.append(offset)
        self.states['length'].append(trial_length)
        self.states['start_times'].append(trial_start_times)
        self.states['color'].append(trial_color)
        self.states['yaxis'].append([self.COUNT] * len(trial_length))
        print(self.states)
        # ---------- POKES: ---------------------------------
        # Note: scatter function of matplotlib doesn't accept a list of markers,
        # and that's why it's necessary to split pokes in from pokes out.
        self.process_pokes(trial, 'in', self.pokes_in)
        self.process_pokes(trial, 'out', self.pokes_out)

        self.COUNT += 1

    def process_current_trial(self, current_band):
        """
        This method receives an icomplete piece of a trial and tries to get information about the states
        looking at the TRANSITION rows. It calls process_pokes() at the end to do the same with the pokes.
        It oftentimes returns an empty structure, because the trial had just started and no information
        can yet be obtained.
        """
        # ------------ STATES: ------------------------------
        transitions = current_band.query("TYPE=='TRANSITION'")['MSG'].values
        times = current_band.query("TYPE=='TRANSITION'")['BPOD-INITIAL-TIME'].values.astype(float)
        current_trial_start = times[:-1]
        current_trial_yaxis = None
        if len(transitions) > 1:
            current_trial_lengths = [times[i+1] - times[i] for i in range(len(times)-1)]
            current_trial_colors = [self.st.state_colors.get(state) for state in transitions[:-1]]
            if self.st.first_state in self.st.state_list:
                current_trial_start = np.insert(current_trial_start, 0, 0)
                current_trial_lengths = np.insert(current_trial_lengths, 0, current_trial_start[1])
                current_trial_colors = np.concatenate((np.array(self.st.state_colors[self.st.first_state]), current_trial_colors), axis=None)
            indices = np.where(np.equal(current_trial_colors, None))[0]
            self.current_trial_states['start_times'] = np.delete(current_trial_start, indices) 
            self.current_trial_states['length'] = np.delete(current_trial_lengths, indices) 
            self.current_trial_states['color'] = np.delete(current_trial_colors, indices) 
            current_trial_yaxis = [self.COUNT] * len(self.current_trial_states['color'])
        self.current_trial_states['yaxis'] = current_trial_yaxis
        # --------------- POKES: ---------------------------------
        self.process_pokes(current_band, 'in', self.current_trial_pokes_in, current=True)
        self.process_pokes(current_band, 'out', self.current_trial_pokes_out, current=True)

    def process_pokes(self, trial, p_type: str, container: dict, current=False):
        """
        This method tries to obtain information about the session pokes by looking at the EVENT rows of the dataframe,
        which is the only way of doing it. Both past and current trials are sent here.
            p_type (poke_type) = 'in' or 'out'.
            container = the dictionary that will hold the data.
            current = if the pokes belong to the current trial or not (or they belong to past trials).
        """
        if p_type == 'in':
            poke_list = ('Port1In', 'Port2In')
        else:
            poke_list = ('Port1Out', 'Port2Out')
        poke_query = []
        poke_color = []
        poke_yaxis = []
        for poke_type in poke_list:
            start_times = trial.loc[(trial['TYPE']=='EVENT') & (trial['+INFO'] == poke_type), 'BPOD-INITIAL-TIME'].values
            start_times = start_times.astype(float)
            poke_query = np.concatenate((poke_query, start_times), axis=None)
            poke_color = np.concatenate((poke_color, [[self.st.poke_colors[poke_type]] * len(start_times)]), axis=None)
            poke_yaxis = np.concatenate((poke_yaxis, [[self.COUNT] * len(start_times)]), axis=None)
        if current:
            container['start_times'] = poke_query
            container['color'] = poke_color
            container['yaxis'] = poke_yaxis
        else:
            container['start_times'].append(poke_query)
            container['color'].append(poke_color)
            container['yaxis'].append(poke_yaxis)

    def create_states_bars(self):
        """
        Creates four arrays by slicing and concatenating the stored lists of lists that hold the previously calculated
        data. The arrays (length, start_times, yaxis and color) make it easy to plot the states in a barh plot.
        The arrays are packed into a dict and sent to the main window.
        """
        aligned_start_times = []
        for array, offset in zip(self.states['start_times'][self.START:self.END], self.offset_record[self.START:self.END]):
            aligned_start_times.append(np.array(array) - offset)
        # Join the relevant arrays:
        try:    
            start_times = np.concatenate(aligned_start_times, axis=None)
            color = np.concatenate(self.states['color'][self.START:self.END], axis=None)
            length = np.concatenate(self.states['length'][self.START:self.END], axis=None)
            yaxis = np.concatenate(self.states['yaxis'][self.START:self.END], axis=None)
        except ValueError:  # To avoid errors at startup
            start_times = []
            color = []
            length = []
            yaxis = []
        # Gather them together into a dict:
        states_plot = {'start_times': start_times, 
                       'length': length, 
                       'color': color, 
                       'yaxis': yaxis}
        return states_plot

    def create_poke_scatter(self, data):
        """
        Same as the previous method, but now we pack data for plotting pokes easily in a scatter plot.
        """
        start_times = data['start_times']
        color = data['color']
        yaxis = data['yaxis']
        aligned_pokes_start = []
        for array, offset in zip(start_times[self.START:self.END], self.offset_record[self.START:self.END]):
            aligned_pokes_start.append(np.array(array) - offset)
        try:
            r_start_times = np.concatenate(aligned_pokes_start, axis=None)
            r_color = np.concatenate(color[self.START:self.END], axis=None)
            r_yaxis = np.concatenate(yaxis[self.START:self.END], axis=None)
        except ValueError:  # To avoid errors at startup
            r_start_times = []
            r_color = []
            r_yaxis = []
        pokes = {'start_times': r_start_times, 
                'color': r_color,
                'yaxis': r_yaxis}
        return pokes

    def calculate_limits(self):
        """
        Updates the END and START numbers according to the user-selected axis size.
        """
        self.END = self.COUNT
        if self.COUNT > self.axis_size:
            self.START = self.COUNT - self.axis_size
        else:
            self.START = 0

    @property
    def past_states(self):
        self.calculate_limits()
        return self.create_states_bars()
    
    @property
    def current_trial(self):
        return self.current_trial_states

    @property 
    def past_pokes(self):
        return self.create_poke_scatter(self.pokes_in), self.create_poke_scatter(self.pokes_out)

    @property
    def current_pokes(self):
        return self.current_trial_pokes_in, self.current_trial_pokes_out


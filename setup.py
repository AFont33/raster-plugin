#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('raster_plugin/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
	                    fd.read(), re.MULTILINE).group(1)

if not version:
	raise RuntimeError('Cannot find version information')

setup(
	name='raster-plugin',
	version=version,
	description="""PyBpod GUI Raster plugin""",
	author='Albert Font',
	author_email='albertf80@gmail.com',
	license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
	url='https://bitbucket.org/azi92rach/raster-plugin',

	include_package_data=True,
	packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

	package_data={'raster_plugin': [
		'resources/*.*',
	]
	},
)

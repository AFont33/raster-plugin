# Plugin for a raster plot visualization #

A plugin for [Pyforms Generic Editor Welcome plugin](https://bitbucket.org/fchampalimaud/pybpod-gui-plugin).
It displays a raster plot of live and recorded BPOD sessions, showing
events (state transitions and pokes) in a trial versus time.

It is tested on Ubuntu with Python 3.

## How to install:

### Plugin
1. On your Desktop, create a new folder on your favorite location (e.g. "plugins")
2. Move this plugin folder to that location
3. Open GUI
4. Select Options > Edit user settings
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'raster_plugin',
    ]
```

6. Save
7. Restart GUI
8. Right-click on a task and select `Raster`.

## How to use

# Configuration
The plugin needs to be set up before using. The configuration file is inside the plugin folder
and it's called `plot_settings.json`. Upon opening it, you will see something like:

```
{
"task_1": {
                   "state_list": ["Punish", "Reward", "Fixation", "WaitCPoke"],
                   "state_colors" : {"Punish": "red", "Reward": "green", "Fixation": "gray", "WaitCPoke": "blue"},
                   "poke_colors" : {"Port1In": "cyan", "Port1Out": "cyan", "Port2In": "blue", "Port2Out": "blue", "Port3In": "magenta", "Port3Out": "magenta"},
                   "poke_markers" : {"in": "o", "out": "|"},
                   "first_state" : "WaitCPoke",
                   "alignment" : "Fixation"
                  },
"task_2": {
                   "state_list": ["Punish", "Reward", "Fixation", "WaitCPoke"],
                   "state_colors" : {"Punish": "black", "Reward": "red", "Fixation": "blue", "WaitCPoke": "green"},
                   "poke_colors" : {"Port1In": "yellow", "Port1Out": "cyan", "Port2In": "blue", "Port2Out": "blue", "Port3In": "magenta", "Port3Out": "magenta"},
                   "poke_markers" : {"in": "|", "out": "D"},
                   "first_state" : "WaitCPoke",
                   "alignment" : "WaitCPoke"
                  }
}
```

Substitute `task_1` and `task_2` for the name of you tasks. You can have as many as you like, but pay attention to how are they formatted inside the JSON file.
Then complete the task information for each of them:
* `state_list` contains the name of the states the you want to visualize in the plot.
* `state_colors` contains the colors that you want to match with each state. Make sure that all the states that you put in the `state_list` have an associated color.
* `poke_colors` contains the colors for the left, center and right pokes. Please be aware that if you use some ports other than 1, 2 and 3 you need to change
the `Port1In`, `Port1Out`, etc, as required (this will we automatically done in the future).
* `poke_markers` are the markers for the pokes using matplotlib's [marker notation](https://matplotlib.org/api/markers_api.html).
* `first_state` is the name of the first state in your state machine. This is necessary to correctly plot the states in the current trial, since during live sessions the only
possible way of knowing the current state is by observing the TRANSITION events, but the first state is given by a default, so there's no way of 
obtaining its name just from reading the session data.
* `alignment` is the name of the state that you want have aligned with 0 in all trials. Make sure that you use a state that appears in ALL trials.

## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`
